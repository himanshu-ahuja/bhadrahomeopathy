package bhadra.com.mca;

/**
 * Created by himanshu on 4/3/18.
 */

public class AddPatient {
    public String imageName;
    public String imageURL;
    private String patientName;
    private String patientGender;
    private String patientDOB;
    private String patientRelation;


    public AddPatient() {
        // Default constructor required for calls to DataSnapshot.getValue(User.class)
    }

    public AddPatient(String patientName, String patientGender, String patientDOB, String patientRelation) {
        this.patientName = patientName;
        this.patientGender = patientGender;
        this.patientDOB = patientDOB;
        this.patientRelation = patientRelation;

    }

    public AddPatient(String name, String url) {

        this.imageName = name;
        this.imageURL= url;
    }
    public String getImageName() {
        return imageName;
    }

    public String getImageURL() {
        return imageURL;
    }

    public String getPatientName() {
        return patientName;
    }

    public void setPatientName(String patientName) {
        this.patientName = patientName;
    }

    public String getPatientGender() {
        return patientGender;
    }

    public void setPatientGender(String patientGender) {
        this.patientGender = patientGender;
    }

    public String getPatientDOB() {
        return patientDOB;
    }

    public void setPatientDOB(String patientDOB) {
        this.patientDOB = patientDOB;
    }

    public String getPatientRelation() {
        return patientRelation;
    }

    public void setPatientRelation(String patientRelation) {
        this.patientRelation = patientRelation;
    }

    @Override
    public String toString() {
        return "AddPatient{" +
                "patientName='" + patientName + '\'' +
                ", patientGender='" + patientGender + '\'' +
                ", patientDOB='" + patientDOB + '\'' +
                ", patientRelation='" + patientRelation + '\'' +
                '}';
    }
}
