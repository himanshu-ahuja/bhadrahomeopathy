package bhadra.com.mca;


import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v7.app.ActionBar;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.util.Calendar;
/*import com.wdullaer.materialdatetimepicker.date.DatePickerDialog;*/


public class NewFamilyMemberFragment extends Fragment {
    //action bar
    ActionBar actionbar;
    TextView textTitle;
    android.app.ActionBar.LayoutParams layoutparams;

    SharedPreferences sharedpreferences;
    Calendar dateSelected = Calendar.getInstance();
    ProgressDialog dialog;
    long maxDateLong;
    long minDateLong;
    private EditText editTextFamilyMemberName;
    private EditText editTextFamilyMemberDOB;
    private String SF_NAME = "docID";
    private RadioButton radioButtonFamilyMemberMale;
    private RadioButton radioButtonFamilyMemberFemale;
    private Button buttonSpinner;
    private Button buttonSubmitPatient;
    private Button buttonCancelPatient;
    private String memberGender = "";
    private String patient_name = "";
    private String patient_dob = "";
    private int add_patient_count = 0;
    private DatePickerDialog datePickerDialog;
    //firebase
    private FirebaseAuth firebaseAuth;
    private DatabaseReference databaseReference;
    private FirebaseUser firebaseUser;

    public NewFamilyMemberFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_new_family_member, container, false);

        firebaseAuth = FirebaseAuth.getInstance();
        firebaseUser = FirebaseAuth.getInstance().getCurrentUser();
        databaseReference = FirebaseDatabase.getInstance().getReference();

        Toast.makeText(getContext(), "USER ID: " + firebaseUser.getUid() + "", Toast.LENGTH_SHORT).show();

        editTextFamilyMemberName = view.findViewById(R.id.editTextPatientName);
        editTextFamilyMemberDOB = view.findViewById(R.id.editTextPatientDOB);
        radioButtonFamilyMemberMale = view.findViewById(R.id.radioButtonFamilyMemberMale);
        radioButtonFamilyMemberFemale = view.findViewById(R.id.radioButtonFamilyMemberFemale);

        final String[] patient_relation = {""};
        buttonSubmitPatient = view.findViewById(R.id.buttonSubmitPatient);
        buttonCancelPatient = view.findViewById(R.id.buttonCancelPatient);

        buttonSpinner = view.findViewById(R.id.buttonSpinner);

        //init calendar to get the current year, month and day
        final Calendar cal = Calendar.getInstance();
        Calendar min = Calendar.getInstance();
        Calendar threemonths = Calendar.getInstance();
        threemonths.set(cal.get(Calendar.YEAR), cal.get(Calendar.MONTH) - 2, cal.get(Calendar.DAY_OF_MONTH));
        min.set(1960, 0, 01);
        minDateLong = min.getTimeInMillis();
        maxDateLong = threemonths.getTimeInMillis();
        final DatePickerDialog.OnDateSetListener dateListener = new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
                int yearSelected = year;
                int monthSelected = month + 1;
                int daySelected = dayOfMonth;
                editTextFamilyMemberDOB.setText("" + dayOfMonth + "/" + monthSelected + "/" + yearSelected + "");
            }

        };
        final DatePickerDialog datePickerDialog = new DatePickerDialog(getContext(),
                dateListener,   //set the listener
                cal.get(Calendar.YEAR), cal.get(Calendar.MONTH), cal.get(Calendar.DAY_OF_MONTH));
        datePickerDialog.getDatePicker().setMinDate(minDateLong);
        datePickerDialog.getDatePicker().setMaxDate(maxDateLong);


        final String[] relations = new String[]{"Husband", "Wife", "Mother", "Father", "Daughter", "Son", "Sister", "Brother"};
        final ArrayAdapter<String> adapter = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_spinner_dropdown_item, relations);

        buttonSpinner.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new AlertDialog.Builder(getActivity())
                        .setTitle("Select Relation")
                        .setAdapter(adapter, new DialogInterface.OnClickListener() {

                            @Override
                            public void onClick(DialogInterface dialog, int which) {

                                String selected = relations[which];
                                patient_relation[0] = selected;
                                //Toast.makeText(getActivity(),"Selected item is: "+selected+"",Toast.LENGTH_SHORT).show();
                                buttonSpinner.setText(selected);
                                dialog.dismiss();
                            }
                        }).create().show();
            }
        });

        //Adding new patient firebase
        buttonSubmitPatient.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                patient_name = editTextFamilyMemberName.getText().toString().trim();
                patient_dob = editTextFamilyMemberDOB.getText().toString().trim();

                if (radioButtonFamilyMemberMale.isChecked()) {
                    memberGender = "Male";
                }
                if (radioButtonFamilyMemberFemale.isChecked()) {
                    memberGender = "Female";
                }
                boolean validate = validateInput(patient_name, memberGender, patient_dob, patient_relation[0]);
                if (!validate) {
                    return;
                } else {
                    writeNewPatient(patient_name, memberGender, patient_dob, patient_relation[0]);
                }
                //writeNewPatient(patient_name, memberGender, patient_dob, patient_relation[0]);
            }
        });

        //datepicker
        editTextFamilyMemberDOB.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //creating the DatePickerDialog

                datePickerDialog.show();
                //new MyEditTextDatePicker(getActivity(), R.id.editTextPatientDOB);
            }
        });
        return view;
    }

    private void writeNewPatient(String patient_name, String gender, String DOB, String relation) {
        final FamilyPatientCount familyPatientCount = new FamilyPatientCount();
        if (add_patient_count > 5) {
            Toast.makeText(getContext(), "Limit Exceeded!", Toast.LENGTH_SHORT).show();
            return;
        }

        sharedpreferences = getContext().getSharedPreferences(SF_NAME,
                Context.MODE_PRIVATE);

        String docID = sharedpreferences.getString("documentID", null);
        Log.d("DOCUMENT_ID: ", "" + docID + "");

        AddPatient addPatient = new AddPatient(patient_name, gender, DOB, relation);

        DatabaseReference rootRef = databaseReference.child("users").child(docID).child("familyMembers");
        DatabaseReference familyRef = rootRef.push();
        familyRef.setValue(addPatient)
                .addOnSuccessListener(new OnSuccessListener<Void>() {
                    @Override
                    public void onSuccess(Void aVoid) {
                        dialog = ProgressDialog.show(getContext(), "Add Patient", "Registering family member", true);
                        add_patient_count++;
                        familyPatientCount.setFamilyPatientCount(add_patient_count);
                        if (dialog != null && dialog.isShowing())
                            dialog.dismiss();
                        Toast.makeText(getContext(), "Patient added!", Toast.LENGTH_SHORT).show();
                        startActivity(new Intent(getContext(), Dashboard.class));
                    }
                })
                .addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        Toast.makeText(getContext(), "Could not add patient. Please try again later", Toast.LENGTH_SHORT).show();
                    }
                });

    }

    public boolean validateInput(String patientName, String gender, String dob, String relation) {

        if (TextUtils.isEmpty(patientName)) {
            editTextFamilyMemberName.setError("Patient Name is required");
            return false;
        }

        if (TextUtils.isEmpty(gender)) {
            Toast.makeText(getContext(), "Please select gender", Toast.LENGTH_SHORT).show();
            return false;
        }
        if (TextUtils.isEmpty(dob)) {
            editTextFamilyMemberDOB.setError("Please select Patient's DOB");
            return false;
        }
        if (TextUtils.isEmpty(relation)) {
            buttonSpinner.setError("Please enter family relation");
            return false;
        }

        return true;
    }
}
