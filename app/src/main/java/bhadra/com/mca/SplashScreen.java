package bhadra.com.mca;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.TextView;

public class SplashScreen extends AppCompatActivity {
    private TextView tv;
    private ImageView image;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash_screen);
        tv = (TextView) findViewById(R.id.headtext);
        image = (ImageView) findViewById(R.id.logo);

        Animation myanim = AnimationUtils.loadAnimation(this, R.anim.splashtransition);
        tv.startAnimation(myanim);
        image.startAnimation(myanim);
        //startActivity(new Intent(SplashScreen.this, LoginActivity.class));
        //finish();
    }
}
