package bhadra.com.mca;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.view.Gravity;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

public class LoginActivity extends AppCompatActivity {

    ActionBar actionbar;
    TextView textTitle;
    android.app.ActionBar.LayoutParams layoutparams;

    private EditText editTextNumber;
    private Button buttonLogin;
    private Button buttonRegister;

    private FirebaseAuth firebaseAuth;


    private void ActionBarTitleGravity() {
        actionbar = getSupportActionBar();
        textTitle = new TextView(getApplicationContext());
        layoutparams = new android.app.ActionBar.LayoutParams(RelativeLayout.LayoutParams.MATCH_PARENT, ActionBar.LayoutParams.WRAP_CONTENT);
        textTitle.setLayoutParams(layoutparams);
        textTitle.setText(R.string.action_sign_in);
        textTitle.setTextColor(Color.WHITE);
        textTitle.setGravity(Gravity.CENTER_HORIZONTAL);
        textTitle.setTextSize(17);
        actionbar.setDisplayOptions(ActionBar.DISPLAY_SHOW_CUSTOM);
        actionbar.setCustomView(textTitle);

    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        firebaseAuth = FirebaseAuth.getInstance();
        if (firebaseAuth.getCurrentUser() != null) {
            Intent mainIntent = new Intent();
            mainIntent.setClass(LoginActivity.this, Dashboard.class);
            startActivity(mainIntent);
            finish();
        }

        ActionBarTitleGravity();

        editTextNumber = findViewById(R.id.editTextNumberLogin);
        buttonLogin = findViewById(R.id.buttonLogin);
        buttonRegister = findViewById(R.id.buttonRegister);

        buttonLogin.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                if (TextUtils.isEmpty(editTextNumber.getText())) {
                    editTextNumber.setError("Phone Number is required");
                    return;
                } else if (!android.util.Patterns.PHONE.matcher(editTextNumber.getText().toString()).matches()) {
                    editTextNumber.setError("Invalid Number Entered");
                    return;
                } else {
                    String number = editTextNumber.getText().toString();
                    checkUserExists(number);
                }
            }
        });

        buttonRegister.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(LoginActivity.this, Registration.class));
                finish();
            }
        });


    }

    public void checkUserExists(final String number) {

        DatabaseReference databaseReference = FirebaseDatabase.getInstance().getReference();
        databaseReference.child("users")
                .orderByChild("mobileNumber")
                .equalTo(number)
                .addValueEventListener(new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {
                        if (dataSnapshot.exists()) {
                            Intent intentOTP = new Intent();
                            intentOTP.setClass(LoginActivity.this, OTPAuth.class);
                            intentOTP.putExtra("mobileNumber", number);
                            intentOTP.putExtra("fromLoginActivity", true);
                            startActivity(intentOTP);
                            return;
                        } else {
                            editTextNumber.setError("This number is not registered");
                            return;
                        }
                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {
                        Toast.makeText(getApplicationContext(), "Firebase error occurred", Toast.LENGTH_SHORT).show();
                        return;
                    }
                });
    }

    @Override
    public void onBackPressed() {
        finish();
    }

}

