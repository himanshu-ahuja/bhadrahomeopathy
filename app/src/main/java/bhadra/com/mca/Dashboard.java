package bhadra.com.mca;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Bundle;
import android.provider.CalendarContract;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.CardView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.PopupMenu;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

public class Dashboard extends AppCompatActivity implements PopupMenu.OnMenuItemClickListener {

    public int patientAddCount = 1;

    PopupMenu popup;
    //action bar
    ActionBar actionbar;
    TextView textTitle;
    android.app.ActionBar.LayoutParams layoutparams;
    Button reports1, reports2, reports3, reports4, reports5, reports6, appointment1, appointment2, appointment3, appointment4, appointment5, appointment6;
    Context context;
    CardView patient1_card, patient2_card, patient3_card, patient4_card, patient5_card, patient6_card;
    TextView textViewPatient1;
    TextView textViewPatient2;
    TextView textViewPatient3;
    TextView textViewPatient4;
    TextView textViewPatient5;
    TextView textViewPatient6;
    TextView textRelation1;
    TextView textRelation2;
    TextView textRelation3;
    TextView textRelation4;
    TextView textRelation5;
    TextView textRelation6;
    SharedPreferences sharedpreferences;
    FirebaseAuth firebaseAuth;
    LinearLayout linearLayout;
    AlertDialog.Builder builder;
    int clicks = 0, token = 0;
    String timeslot = "17:22";
    String timeadd = "00:08";
    Tokens tok;
    int hours, minutes;
    Calendar cal;
    long calID = 3;
    long startMillis = 0;
    long endMillis = 0;
    String _idFamilyMember[] = new String[10];
    int i = 1;
    int buttonClicked = 0;
    ProgressDialog dialog;
    private String SF_NAME = "docID";

    private void ActionBarTitleGravity() {
        actionbar = getSupportActionBar();
        textTitle = new TextView(getApplicationContext());
        layoutparams = new android.app.ActionBar.LayoutParams(RelativeLayout.LayoutParams.MATCH_PARENT, ActionBar.LayoutParams.WRAP_CONTENT);
        textTitle.setLayoutParams(layoutparams);
        textTitle.setText(R.string.title_activity_dashboard);
        textTitle.setTextColor(Color.WHITE);
        textTitle.setGravity(Gravity.CENTER_HORIZONTAL);
        textTitle.setTextSize(17);
        actionbar.setDisplayOptions(ActionBar.DISPLAY_SHOW_CUSTOM);
        actionbar.setCustomView(textTitle);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dashboard);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        ActionBarTitleGravity();

        sharedpreferences = getApplicationContext().getSharedPreferences(SF_NAME,
                Context.MODE_PRIVATE);

        final String docID = sharedpreferences.getString("documentID", null);
        Log.d("DOCUMENT_ID: ", "" + docID + "");


        context = getApplicationContext();
        firebaseAuth = FirebaseAuth.getInstance();
        linearLayout = (LinearLayout) findViewById(R.id.layout_linear);
        patient1_card = (CardView) findViewById(R.id.card_patientdetails);
        patient2_card = (CardView) findViewById(R.id.card_patientdetails2);
        patient3_card = (CardView) findViewById(R.id.card_patientdetails3);
        patient4_card = (CardView) findViewById(R.id.card_patientdetails4);
        patient5_card = (CardView) findViewById(R.id.card_patientdetails5);
        patient6_card = (CardView) findViewById(R.id.card_patientdetails6);
        textViewPatient1 = findViewById(R.id.txt_patientname);
        textViewPatient2 = findViewById(R.id.txt_patientname2);
        textViewPatient3 = findViewById(R.id.txt_patientname3);
        textViewPatient4 = findViewById(R.id.txt_patientname4);
        textViewPatient5 = findViewById(R.id.txt_patientname5);
        textViewPatient6 = findViewById(R.id.txt_patientname6);
        textRelation1 = findViewById(R.id.txt_patientrelation);
        textRelation2 = findViewById(R.id.txt_patientrelation2);
        textRelation3 = findViewById(R.id.txt_patientrelation3);
        textRelation4 = findViewById(R.id.txt_patientrelation4);
        textRelation5 = findViewById(R.id.txt_patientrelation5);
        textRelation6 = findViewById(R.id.txt_patientrelation6);

        reports1 = (Button) findViewById(R.id.btn_reports);
        reports2 = (Button) findViewById(R.id.btn_reports2);
        reports3 = (Button) findViewById(R.id.btn_reports3);
        reports4 = (Button) findViewById(R.id.btn_reports4);
        reports5 = (Button) findViewById(R.id.btn_reports5);
        reports6 = (Button) findViewById(R.id.btn_reports6);
        appointment1 = (Button) findViewById(R.id.btn_appointment);
        appointment2 = (Button) findViewById(R.id.btn_appointment2);
        appointment3 = (Button) findViewById(R.id.btn_appointment3);
        appointment4 = (Button) findViewById(R.id.btn_appointment4);
        appointment5 = (Button) findViewById(R.id.btn_appointment5);
        appointment6 = (Button) findViewById(R.id.btn_appointment6);

        builder = new AlertDialog.Builder(this);

        tok = new Tokens();

        //Toast.makeText(getApplicationContext(), "UserID: " + firebaseAuth.getCurrentUser().getUid() + "", Toast.LENGTH_SHORT).show();
        /*Toast.makeText(getApplicationContext(), "docID:" + docID + "", Toast.LENGTH_SHORT).show();
        Log.d("docID: ", "" + docID + "");
*/
        //Loading head name
        DatabaseReference databaseReference = FirebaseDatabase.getInstance().getReference();
        DatabaseReference ref = databaseReference.child("users");
        dialog = ProgressDialog.show(Dashboard.this, "Loading Dashboard", "Fetching Details", true);
        ref.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {

                Log.d("CHILDREN_COUNT: ", "" + String.valueOf(dataSnapshot.getChildrenCount()) + "");
                for (DataSnapshot userSnapshot : dataSnapshot.getChildren()) {
                   // String name = userSnapshot.child("familyHeadName").getValue(String.class);
                    Log.d("docID",docID);
                    //Log.d("snapshot:",userSnapshot.child(docID).getValue(String.class));
                    String head_name = userSnapshot.child("familyHeadName").getValue(String.class);
                    Log.d("NAME: ", "" + head_name + "");
                    textViewPatient1.setText("" + head_name);

                    //Log.d("DATA", "" + userSnapshot.getValue(userClass.getClass()) + "");
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                Log.d("EXCEPTION", "onCancelled", databaseError.toException());

            }

        });

        //Loading patient family member details
        DatabaseReference familyRef = FirebaseDatabase.getInstance().getReference();
        DatabaseReference ref2 = familyRef.child("users").child(docID).child("familyMembers");
        ref2.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                _idFamilyMember[0] = docID;
                //dialog = ProgressDialog.show(getApplicationContext(), "Loading Dashboard", "Fetching Details", true);
                Log.d("CHILDREN_COUNT_F:", "" + String.valueOf(dataSnapshot.getChildrenCount()) + "");
                long familyMemberCount = dataSnapshot.getChildrenCount(); // for loop iteration & card visibility
                    if(familyMemberCount == 0){
                        if(dialog!= null && dialog.isShowing())
                            dialog.dismiss();
                            return;
                    }
                //String _idFamilyMember[] = new String[(int) familyMemberCount];
                int loop_count = 0; // for setting textview
                if (familyMemberCount == 1) {
                    patient2_card.setVisibility(View.VISIBLE);
                }
                if (familyMemberCount == 2) {
                    patient2_card.setVisibility(View.VISIBLE);
                    patient3_card.setVisibility(View.VISIBLE);
                }
                if (familyMemberCount == 3) {
                    patient2_card.setVisibility(View.VISIBLE);
                    patient3_card.setVisibility(View.VISIBLE);
                    patient4_card.setVisibility(View.VISIBLE);
                }
                if (familyMemberCount == 4) {
                    patient2_card.setVisibility(View.VISIBLE);
                    patient3_card.setVisibility(View.VISIBLE);
                    patient4_card.setVisibility(View.VISIBLE);
                    patient5_card.setVisibility(View.VISIBLE);

                }
                if (familyMemberCount == 5) {
                    patient2_card.setVisibility(View.VISIBLE);
                    patient3_card.setVisibility(View.VISIBLE);
                    patient4_card.setVisibility(View.VISIBLE);
                    patient5_card.setVisibility(View.VISIBLE);
                    patient6_card.setVisibility(View.VISIBLE);
                }
                for (DataSnapshot userSnapshot : dataSnapshot.getChildren()) {
                    AddPatient patientClass = userSnapshot.getValue(AddPatient.class);
                    Log.d("class:", "" + patientClass.toString() + "");
                    _idFamilyMember[i] = userSnapshot.getKey();
                    Log.d("_CHILDID:", "" + _idFamilyMember[loop_count] +  "");

                    if (familyMemberCount == 1) {
                        if (loop_count == 0) {
                            textViewPatient2.setText("" + patientClass.getPatientName());
                            textRelation2.setText("" + patientClass.getPatientRelation());
                        }
                    }
                    if (familyMemberCount == 2) {
                        if (loop_count == 0) {
                            textViewPatient2.setText("" + patientClass.getPatientName());
                            textRelation2.setText("" + patientClass.getPatientRelation());
                        }
                        if (loop_count == 1) {
                            textViewPatient3.setText("" + patientClass.getPatientName());
                            textRelation3.setText("" + patientClass.getPatientRelation());
                        }
                    }

                    if (familyMemberCount == 3) {
                        if (loop_count == 0) {
                            textViewPatient2.setText("" + patientClass.getPatientName());
                            textRelation2.setText("" + patientClass.getPatientRelation());
                        }
                        if (loop_count == 1) {
                            textViewPatient3.setText("" + patientClass.getPatientName());
                            textRelation3.setText("" + patientClass.getPatientRelation());
                        }
                        if (loop_count == 2) {
                            textViewPatient4.setText("" + patientClass.getPatientName());
                            textRelation4.setText("" + patientClass.getPatientRelation());
                        }
                    }

                    if (familyMemberCount == 4) {
                        if (loop_count == 0) {
                            textViewPatient2.setText("" + patientClass.getPatientName());
                            textRelation2.setText("" + patientClass.getPatientRelation());
                        }
                        if (loop_count == 1) {
                            textViewPatient3.setText("" + patientClass.getPatientName());
                            textRelation3.setText("" + patientClass.getPatientRelation());
                        }
                        if (loop_count == 2) {
                            textViewPatient4.setText("" + patientClass.getPatientName());
                            textRelation4.setText("" + patientClass.getPatientRelation());
                        }
                        if (loop_count == 3) {
                            textViewPatient5.setText("" + patientClass.getPatientName());
                            textRelation5.setText("" + patientClass.getPatientRelation());
                        }
                    }

                    if (familyMemberCount == 5) {
                        if (loop_count == 0) {
                            textViewPatient2.setText("" + patientClass.getPatientName());
                            textRelation2.setText("" + patientClass.getPatientRelation());
                        }
                        if (loop_count == 1) {
                            textViewPatient3.setText("" + patientClass.getPatientName());
                            textRelation3.setText("" + patientClass.getPatientRelation());
                        }
                        if (loop_count == 2) {
                            textViewPatient4.setText("" + patientClass.getPatientName());
                            textRelation4.setText("" + patientClass.getPatientRelation());
                        }
                        if (loop_count == 3) {
                            textViewPatient3.setText("" + patientClass.getPatientName());
                            textRelation3.setText("" + patientClass.getPatientRelation());
                        }
                        if (loop_count == 4) {
                            textViewPatient5.setText("" + patientClass.getPatientName());
                            textRelation5.setText("" + patientClass.getPatientRelation());
                        }
                        if (loop_count == 5) {
                            textViewPatient6.setText("" + patientClass.getPatientName());
                            textRelation6.setText("" + patientClass.getPatientRelation());
                        }
                    }
                    loop_count++;
                    i++;
                }
                if(dialog!= null && dialog.isShowing())
                    dialog.dismiss();
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                Log.d("EXCEPTION", "onCancelled", databaseError.toException());
            }
        });


        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                FamilyPatientCount familyPatientCount = new FamilyPatientCount();
                if (familyPatientCount.getFamilyPatientCount() > 5) {
                    Toast.makeText(getApplicationContext(), "Limit Exceeded! Create new account to add more family members.", Toast.LENGTH_SHORT).show();
                    return;
                }
                Fragment fragment = null;
                Class fragmentClass = null;
                fragmentClass = NewFamilyMemberFragment.class;

                try {
                    fragment = (Fragment) fragmentClass.newInstance();
                } catch (Exception e) {
                    e.printStackTrace();
                }

                FragmentManager fragmentManager = getSupportFragmentManager();
                fragmentManager.beginTransaction().replace(R.id.frameLayoutContent, fragment).commit();
              /*  if (cardview2.getVisibility() == View.INVISIBLE)
                    cardview2.setVisibility(View.VISIBLE);

                else if (cardview3.getVisibility() == View.INVISIBLE)
                    cardview3.setVisibility(View.VISIBLE);

                else if (cardview4.getVisibility() == View.INVISIBLE)
                    cardview4.setVisibility(View.VISIBLE);

                else if (cardview5.getVisibility() == View.INVISIBLE)
                    cardview5.setVisibility(View.VISIBLE);

                else
                    cardview6.setVisibility(View.VISIBLE);*/
            }
        });

        reports1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                buttonClicked = 0;
                showPopUp();
                popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                    @Override
                    public boolean onMenuItemClick(MenuItem item) {
                        int id = item.getItemId();
                        switch (id){
                            case R.id.menu_uploadReport:
                                Intent i = new Intent(getApplicationContext(), Upload.class);
                                i.putExtra("child_document_id",_idFamilyMember[0]);
                                i.putExtra("doc_id",docID);
                                startActivity(i);
                                return  true;

                            case R.id.menu_viewReport:
                                Intent viewReportIntent = new Intent(getApplicationContext(), DisplayReport.class);
                                viewReportIntent.putExtra("child_document_id", _idFamilyMember[0]);
                                viewReportIntent.putExtra("doc_id",docID);
                                startActivity(viewReportIntent);
                                return true;
                        }
                        return false;
                    }
                });
            }
        });

        reports2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                buttonClicked = 1;
                showPopUp();
                popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                    @Override
                    public boolean onMenuItemClick(MenuItem item) {
                        int id = item.getItemId();
                        switch (id){
                            case R.id.menu_uploadReport:
                                Intent i = new Intent(getApplicationContext(), Upload.class);
                                i.putExtra("child_document_id",_idFamilyMember[1]);
                                i.putExtra("doc_id",docID);
                                startActivity(i);
                                return  true;

                            case R.id.menu_viewReport:
                                Intent viewReportIntent = new Intent(getApplicationContext(), DisplayReport.class);
                                viewReportIntent.putExtra("child_document_id", _idFamilyMember[1]);
                                viewReportIntent.putExtra("doc_id",docID);
                                startActivity(viewReportIntent);
                                return true;
                        }
                        return false;
                    }
                });

            }
        });

        reports3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                buttonClicked = 2;
                showPopUp();
                popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                    @Override
                    public boolean onMenuItemClick(MenuItem item) {
                        int id = item.getItemId();
                        switch (id){
                            case R.id.menu_uploadReport:
                                Intent i = new Intent(getApplicationContext(), Upload.class);
                                i.putExtra("child_document_id",_idFamilyMember[2]);
                                i.putExtra("doc_id",docID);
                                startActivity(i);
                                return  true;

                            case R.id.menu_viewReport:
                                Intent viewReportIntent = new Intent(getApplicationContext(), DisplayReport.class);
                                viewReportIntent.putExtra("child_document_id", _idFamilyMember[2]);
                                viewReportIntent.putExtra("doc_id",docID);
                                startActivity(viewReportIntent);
                                return true;
                        }
                        return false;
                    }
                });

            }
        });

        reports4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                buttonClicked = 3;
                showPopUp();
                popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                    @Override
                    public boolean onMenuItemClick(MenuItem item) {
                        int id = item.getItemId();
                        switch (id){
                            case R.id.menu_uploadReport:
                                Intent i = new Intent(getApplicationContext(), Upload.class);
                                i.putExtra("child_document_id",_idFamilyMember[3]);
                                i.putExtra("doc_id",docID);
                                startActivity(i);
                                return  true;

                            case R.id.menu_viewReport:
                                Intent viewReportIntent = new Intent(getApplicationContext(), DisplayReport.class);
                                viewReportIntent.putExtra("child_document_id", _idFamilyMember[3]);
                                viewReportIntent.putExtra("doc_id",docID);
                                startActivity(viewReportIntent);
                                return true;
                        }
                        return false;
                    }
                });
            }
        });

        reports5.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                buttonClicked = 4;
                showPopUp();
                popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                    @Override
                    public boolean onMenuItemClick(MenuItem item) {
                        int id = item.getItemId();
                        switch (id){
                            case R.id.menu_uploadReport:
                                Intent i = new Intent(getApplicationContext(), Upload.class);
                                i.putExtra("child_document_id",_idFamilyMember[4]);
                                i.putExtra("doc_id",docID);
                                startActivity(i);
                                return  true;

                            case R.id.menu_viewReport:
                                Intent viewReportIntent = new Intent(getApplicationContext(), DisplayReport.class);
                                viewReportIntent.putExtra("child_document_id", _idFamilyMember[4]);
                                viewReportIntent.putExtra("doc_id",docID);
                                startActivity(viewReportIntent);
                                return true;
                        }
                        return false;
                    }
                });
            }
        });

        reports6.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                buttonClicked = 5;
                showPopUp();
                popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                    @Override
                    public boolean onMenuItemClick(MenuItem item) {
                        int id = item.getItemId();
                        switch (id){
                            case R.id.menu_uploadReport:
                                Intent i = new Intent(getApplicationContext(), Upload.class);
                                i.putExtra("child_document_id",_idFamilyMember[5]);
                                i.putExtra("doc_id",docID);
                                startActivity(i);
                                return  true;

                            case R.id.menu_viewReport:
                                Intent viewReportIntent = new Intent(getApplicationContext(), DisplayReport.class);
                                viewReportIntent.putExtra("child_document_id", _idFamilyMember[5]);
                                viewReportIntent.putExtra("doc_id",docID);
                                startActivity(viewReportIntent);
                                return true;
                        }
                        return false;
                    }
                });
            }
        });

        appointment1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                clicks++;
                token++;
                tok.setTokenno(token);
                DateFormat df = new SimpleDateFormat("yyyy/MM/dd");
                Date date = new Date();
                String today = df.format(date);
                String split [] = today.split("/");
                String yearstr = split[0];
                String monthstr = split[1];
                String daystr = split[2];
                int year = Integer.parseInt(yearstr);
                int month = Integer.parseInt(monthstr);
                int day = Integer.parseInt(daystr);

                String[] firstTimeParts = timeslot.split(":");
                // Converting String to Integer
                final int hours1 = Integer.parseInt(firstTimeParts[0]);
                final int minutes1 = Integer.parseInt(firstTimeParts[1]);

                // Separating second String using delimiter ":"
                String[] secondTimeParts = timeadd.split(":");
                // Converting String to Integer
                final int hours2 = Integer.parseInt(secondTimeParts[0]);
                final int minutes2 = Integer.parseInt(secondTimeParts[1]);

                hours = hours1 + hours2;
                minutes = minutes1 + minutes2;

                if (minutes > 59) {
                    minutes = minutes - 60;
                    hours = hours + 1;
                    if (hours > 23) {
                        hours = hours - 24;
                    }
                } else {

                    if (hours > 23) {
                        hours = hours - 24;
                    }
                }
                cal = Calendar.getInstance();
                cal.set(year, month-1, day, hours, minutes);
                startMillis = cal.getTimeInMillis();

                timeslot = String.valueOf(hours) + ":" + String.valueOf(minutes);
                tok.setTime(timeslot);

                if(clicks==1) {
                    timeslot= tok.getTime();
                    token = tok.getTokenno();
                }

                if(clicks==2) {
                    timeslot = tok.getTime();
                    token = tok.getTokenno();
                }

                if(clicks==3) {
                    timeslot = tok.getTime();
                    token = tok.getTokenno();
                }

                if(clicks==4) {
                    timeslot = tok.getTime();
                    token = tok.getTokenno();
                }

                if(clicks==5) {
                    timeslot = tok.getTime();
                    token = tok.getTokenno();
                }

                if(clicks==6) {
                    timeslot = tok.getTime();
                    token = tok.getTokenno();
                }
                if(hours==19 && minutes>30) {
                    Toast.makeText(context, "Appointment Unavailable", Toast.LENGTH_SHORT).show();
                }
                else {
                    builder.setTitle("Book Appointment")
                            .setMessage("Your token no is " + token + " and time slot is " + timeslot + ". Do you want to confirm the appointment?")
                            .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int which) {
                                    Toast.makeText(context, "Appointment Confirmed", Toast.LENGTH_SHORT).show();
                                    appointment1.setClickable(false);
                                    appointment1.setAlpha(.5f);

                                    Intent intent = new Intent(Intent.ACTION_INSERT)
                                            .setData(CalendarContract.Events.CONTENT_URI)
                                            .putExtra(CalendarContract.EXTRA_EVENT_BEGIN_TIME, startMillis)
                                            .putExtra(CalendarContract.Events.TITLE, "Appointment")
                                            .putExtra(CalendarContract.Events.DESCRIPTION, "Go to see doctor")
                                            .putExtra(CalendarContract.Events.EVENT_LOCATION, "Bhadra Homeopathy Clinic")
                                            .putExtra(CalendarContract.Events.AVAILABILITY, CalendarContract.Events.AVAILABILITY_BUSY);

                                    startActivity(intent);


                                }
                            })
                            .setNegativeButton(android.R.string.no, new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int which) {
                                    token--;
                                    hours = hours1 - hours2;
                                    minutes = minutes1;
                                    if (minutes > 59) {
                                        minutes = minutes - 60;
                                        hours = hours + 1;
                                        if (hours > 23) {
                                            hours = hours - 24;
                                        }
                                    } else {

                                        if (hours > 23) {
                                            hours = hours - 24;
                                        }
                                    }
                                    timeslot = String.valueOf(hours) + ":" + String.valueOf(minutes);
                                }
                            })
                            .show();
                }
            }
        });

        appointment2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                clicks++;
                token++;
                tok.setTokenno(token);
                DateFormat df = new SimpleDateFormat("yyyy/MM/dd");
                Date date = new Date();
                String today = df.format(date);
                String split [] = today.split("/");
                String yearstr = split[0];
                String monthstr = split[1];
                String daystr = split[2];
                int year = Integer.parseInt(yearstr);
                int month = Integer.parseInt(monthstr);
                int day = Integer.parseInt(daystr);
                String[] firstTimeParts = timeslot.split(":");
                // Converting String to Integer
                final int hours1 = Integer.parseInt(firstTimeParts[0]);
                final int minutes1 = Integer.parseInt(firstTimeParts[1]);

                // Separating second String using delimiter ":"
                String[] secondTimeParts = timeadd.split(":");
                // Converting String to Integer
                final int hours2 = Integer.parseInt(secondTimeParts[0]);
                final int minutes2 = Integer.parseInt(secondTimeParts[1]);

                hours = hours1 + hours2;
                minutes = minutes1 + minutes2;

                if (minutes > 59) {
                    minutes = minutes - 60;
                    hours = hours + 1;
                    if (hours > 23) {
                        hours = hours - 24;
                    }
                } else {

                    if (hours > 23) {
                        hours = hours - 24;
                    }
                }
                cal = Calendar.getInstance();
                cal.set(year, month-1, day, hours, minutes);
                startMillis = cal.getTimeInMillis();
                timeslot = String.valueOf(hours) + ":" + String.valueOf(minutes);
                tok.setTime(timeslot);

                if(clicks==1) {
                    timeslot= tok.getTime();
                    token = tok.getTokenno();
                }

                if(clicks==2) {
                    timeslot = tok.getTime();
                    token = tok.getTokenno();
                }

                if(clicks==3) {
                    timeslot = tok.getTime();
                    token = tok.getTokenno();
                }

                if(clicks==4) {
                    timeslot = tok.getTime();
                    token = tok.getTokenno();
                }

                if(clicks==5) {
                    timeslot = tok.getTime();
                    token = tok.getTokenno();
                }

                if(clicks==6) {
                    timeslot = tok.getTime();
                    token = tok.getTokenno();
                }
                if(hours==19 && minutes>30) {
                    Toast.makeText(context, "Appointment Unavailable", Toast.LENGTH_SHORT).show();
                }
                else {
                    builder.setTitle("Book Appointment")
                            .setMessage("Your token no is " + token + " and time slot is " + timeslot + ". Do you want to confirm the appointment?")
                            .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int which) {
                                    Toast.makeText(context, "Appointment Confirmed", Toast.LENGTH_SHORT).show();
                                    appointment2.setClickable(false);
                                    appointment2.setAlpha(.5f);
                                    cal = Calendar.getInstance();

                                }
                            })
                            .setNegativeButton(android.R.string.no, new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int which) {
                                    token--;
                                    hours = hours1 - hours2;
                                    minutes = minutes1;
                                    if (minutes > 59) {
                                        minutes = minutes - 60;
                                        hours = hours + 1;
                                        if (hours > 23) {
                                            hours = hours - 24;
                                        }
                                    } else {

                                        if (hours > 23) {
                                            hours = hours - 24;
                                        }
                                    }
                                    timeslot = String.valueOf(hours) + ":" + String.valueOf(minutes);
                                }
                            })
                            .show();
                }
            }
        });

        appointment3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                clicks++;
                token++;
                tok.setTokenno(token);
                DateFormat df = new SimpleDateFormat("yyyy/MM/dd");
                Date date = new Date();
                String today = df.format(date);
                String split [] = today.split("/");
                String yearstr = split[0];
                String monthstr = split[1];
                String daystr = split[2];
                int year = Integer.parseInt(yearstr);
                int month = Integer.parseInt(monthstr);
                int day = Integer.parseInt(daystr);
                String[] firstTimeParts = timeslot.split(":");
                // Converting String to Integer
                final int hours1 = Integer.parseInt(firstTimeParts[0]);
                final int minutes1 = Integer.parseInt(firstTimeParts[1]);

                // Separating second String using delimiter ":"
                String[] secondTimeParts = timeadd.split(":");
                // Converting String to Integer
                final int hours2 = Integer.parseInt(secondTimeParts[0]);
                final int minutes2 = Integer.parseInt(secondTimeParts[1]);

                hours = hours1 + hours2;
                minutes = minutes1 + minutes2;

                if (minutes > 59) {
                    minutes = minutes - 60;
                    hours = hours + 1;
                    if (hours > 23) {
                        hours = hours - 24;
                    }
                } else {

                    if (hours > 23) {
                        hours = hours - 24;
                    }
                }
                cal = Calendar.getInstance();
                cal.set(year, month-1, day, hours, minutes);
                startMillis = cal.getTimeInMillis();

                timeslot = String.valueOf(hours) + ":" + String.valueOf(minutes);
                tok.setTime(timeslot);

                if(clicks==1) {
                    timeslot= tok.getTime();
                    token = tok.getTokenno();
                }

                if(clicks==2) {
                    timeslot = tok.getTime();
                    token = tok.getTokenno();
                }

                if(clicks==3) {
                    timeslot = tok.getTime();
                    token = tok.getTokenno();
                }

                if(clicks==4) {
                    timeslot = tok.getTime();
                    token = tok.getTokenno();
                }

                if(clicks==5) {
                    timeslot = tok.getTime();
                    token = tok.getTokenno();
                }

                if(clicks==6) {
                    timeslot = tok.getTime();
                    token = tok.getTokenno();
                }
                if(hours==19 && minutes>30) {
                    Toast.makeText(context, "Appointment Unavailable", Toast.LENGTH_SHORT).show();
                }
                else {
                    builder.setTitle("Book Appointment")
                            .setMessage("Your token no is " + token + " and time slot is " + timeslot + ". Do you want to confirm the appointment?")
                            .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int which) {
                                    Toast.makeText(context, "Appointment Confirmed", Toast.LENGTH_SHORT).show();
                                    appointment3.setClickable(false);
                                    appointment3.setAlpha(.5f);
                                }
                            })
                            .setNegativeButton(android.R.string.no, new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int which) {
                                    token--;
                                    hours = hours1 - hours2;
                                    minutes = minutes1;
                                    if (minutes > 59) {
                                        minutes = minutes - 60;
                                        hours = hours + 1;
                                        if (hours > 23) {
                                            hours = hours - 24;
                                        }
                                    } else {

                                        if (hours > 23) {
                                            hours = hours - 24;
                                        }
                                    }
                                    timeslot = String.valueOf(hours) + ":" + String.valueOf(minutes);
                                }
                            })
                            .show();
                }
            }
        });

        appointment4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                clicks++;
                token++;
                tok.setTokenno(token);
                DateFormat df = new SimpleDateFormat("yyyy/MM/dd");
                Date date = new Date();
                String today = df.format(date);
                String split [] = today.split("/");
                String yearstr = split[0];
                String monthstr = split[1];
                String daystr = split[2];
                int year = Integer.parseInt(yearstr);
                int month = Integer.parseInt(monthstr);
                int day = Integer.parseInt(daystr);
                String[] firstTimeParts = timeslot.split(":");
                // Converting String to Integer
                final int hours1 = Integer.parseInt(firstTimeParts[0]);
                final int minutes1 = Integer.parseInt(firstTimeParts[1]);

                // Separating second String using delimiter ":"
                String[] secondTimeParts = timeadd.split(":");
                // Converting String to Integer
                final int hours2 = Integer.parseInt(secondTimeParts[0]);
                final int minutes2 = Integer.parseInt(secondTimeParts[1]);

                hours = hours1 + hours2;
                minutes = minutes1 + minutes2;

                if (minutes > 59) {
                    minutes = minutes - 60;
                    hours = hours + 1;
                    if (hours > 23) {
                        hours = hours - 24;
                    }
                } else {

                    if (hours > 23) {
                        hours = hours - 24;
                    }
                }
                cal = Calendar.getInstance();
                cal.set(year, month-1, day, hours, minutes);
                startMillis = cal.getTimeInMillis();
                timeslot = String.valueOf(hours) + ":" + String.valueOf(minutes);
                tok.setTime(timeslot);

                if(clicks==1) {
                    timeslot= tok.getTime();
                    token = tok.getTokenno();
                }

                if(clicks==2) {
                    timeslot = tok.getTime();
                    token = tok.getTokenno();
                }

                if(clicks==3) {
                    timeslot = tok.getTime();
                    token = tok.getTokenno();
                }

                if(clicks==4) {
                    timeslot = tok.getTime();
                    token = tok.getTokenno();
                }

                if(clicks==5) {
                    timeslot = tok.getTime();
                    token = tok.getTokenno();
                }

                if(clicks==6) {
                    timeslot = tok.getTime();
                    token = tok.getTokenno();
                }
                if(hours==19 && minutes>30) {
                    Toast.makeText(context, "Appointment Unavailable", Toast.LENGTH_SHORT).show();
                }
                else {
                    builder.setTitle("Book Appointment")
                            .setMessage("Your token no is " + token + " and time slot is " + timeslot + ". Do you want to confirm the appointment?")
                            .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int which) {
                                    Toast.makeText(context, "Appointment Confirmed", Toast.LENGTH_SHORT).show();
                                    appointment4.setClickable(false);
                                    appointment4.setAlpha(.5f);
                                }
                            })
                            .setNegativeButton(android.R.string.no, new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int which) {
                                    token--;
                                    hours = hours1 - hours2;
                                    minutes = minutes1;
                                    if (minutes > 59) {
                                        minutes = minutes - 60;
                                        hours = hours + 1;
                                        if (hours > 23) {
                                            hours = hours - 24;
                                        }
                                    } else {

                                        if (hours > 23) {
                                            hours = hours - 24;
                                        }
                                    }
                                    timeslot = String.valueOf(hours) + ":" + String.valueOf(minutes);
                                }
                            })
                            .show();
                }
            }
        });

        appointment5.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                clicks++;
                token++;
                tok.setTokenno(token);
                DateFormat df = new SimpleDateFormat("yyyy/MM/dd");
                Date date = new Date();
                String today = df.format(date);
                String split [] = today.split("/");
                String yearstr = split[0];
                String monthstr = split[1];
                String daystr = split[2];
                int year = Integer.parseInt(yearstr);
                int month = Integer.parseInt(monthstr);
                int day = Integer.parseInt(daystr);
                String[] firstTimeParts = timeslot.split(":");
                // Converting String to Integer
                final int hours1 = Integer.parseInt(firstTimeParts[0]);
                final int minutes1 = Integer.parseInt(firstTimeParts[1]);

                // Separating second String using delimiter ":"
                String[] secondTimeParts = timeadd.split(":");
                // Converting String to Integer
                final int hours2 = Integer.parseInt(secondTimeParts[0]);
                final int minutes2 = Integer.parseInt(secondTimeParts[1]);

                hours = hours1 + hours2;
                minutes = minutes1 + minutes2;

                if (minutes > 59) {
                    minutes = minutes - 60;
                    hours = hours + 1;
                    if (hours > 23) {
                        hours = hours - 24;
                    }
                } else {

                    if (hours > 23) {
                        hours = hours - 24;
                    }
                }

                timeslot = String.valueOf(hours) + ":" + String.valueOf(minutes);
                tok.setTime(timeslot);

                if(clicks==1) {
                    timeslot= tok.getTime();
                    token = tok.getTokenno();
                }

                if(clicks==2) {
                    timeslot = tok.getTime();
                    token = tok.getTokenno();
                }

                if(clicks==3) {
                    timeslot = tok.getTime();
                    token = tok.getTokenno();
                }

                if(clicks==4) {
                    timeslot = tok.getTime();
                    token = tok.getTokenno();
                }

                if(clicks==5) {
                    timeslot = tok.getTime();
                    token = tok.getTokenno();
                }

                if(clicks==6) {
                    timeslot = tok.getTime();
                    token = tok.getTokenno();
                }
                if(hours==19 && minutes>30) {
                    Toast.makeText(context, "Appointment Unavailable", Toast.LENGTH_SHORT).show();
                }
                else {
                    builder.setTitle("Book Appointment")
                            .setMessage("Your token no is " + token + " and time slot is " + timeslot + ". Do you want to confirm the appointment?")
                            .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int which) {
                                    Toast.makeText(context, "Appointment Confirmed", Toast.LENGTH_SHORT).show();
                                    appointment5.setClickable(false);
                                    appointment5.setAlpha(.5f);
                                }
                            })
                            .setNegativeButton(android.R.string.no, new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int which) {
                                    token--;
                                    hours = hours1 - hours2;
                                    minutes = minutes1;
                                    if (minutes > 59) {
                                        minutes = minutes - 60;
                                        hours = hours + 1;
                                        if (hours > 23) {
                                            hours = hours - 24;
                                        }
                                    } else {

                                        if (hours > 23) {
                                            hours = hours - 24;
                                        }
                                    }
                                    timeslot = String.valueOf(hours) + ":" + String.valueOf(minutes);
                                }
                            })
                            .show();
                }
            }
        });

        appointment6.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                clicks++;
                token++;
                tok.setTokenno(token);
                DateFormat df = new SimpleDateFormat("yyyy/MM/dd");
                Date date = new Date();
                String today = df.format(date);
                String split [] = today.split("/");
                String yearstr = split[0];
                String monthstr = split[1];
                String daystr = split[2];
                int year = Integer.parseInt(yearstr);
                int month = Integer.parseInt(monthstr);
                int day = Integer.parseInt(daystr);
                String[] firstTimeParts = timeslot.split(":");
                // Converting String to Integer
                final int hours1 = Integer.parseInt(firstTimeParts[0]);
                final int minutes1 = Integer.parseInt(firstTimeParts[1]);

                // Separating second String using delimiter ":"
                String[] secondTimeParts = timeadd.split(":");
                // Converting String to Integer
                final int hours2 = Integer.parseInt(secondTimeParts[0]);
                final int minutes2 = Integer.parseInt(secondTimeParts[1]);

                hours = hours1 + hours2;
                minutes = minutes1 + minutes2;

                if (minutes > 59) {
                    minutes = minutes - 60;
                    hours = hours + 1;
                    if (hours > 23) {
                        hours = hours - 24;
                    }
                } else {

                    if (hours > 23) {
                        hours = hours - 24;
                    }
                }

                timeslot = String.valueOf(hours) + ":" + String.valueOf(minutes);
                tok.setTime(timeslot);

                if(clicks==1) {
                    timeslot= tok.getTime();
                    token = tok.getTokenno();
                }

                if(clicks==2) {
                    timeslot = tok.getTime();
                    token = tok.getTokenno();
                }

                if(clicks==3) {
                    timeslot = tok.getTime();
                    token = tok.getTokenno();
                }

                if(clicks==4) {
                    timeslot = tok.getTime();
                    token = tok.getTokenno();
                }

                if(clicks==5) {
                    timeslot = tok.getTime();
                    token = tok.getTokenno();
                }

                if(clicks==6) {
                    timeslot = tok.getTime();
                    token = tok.getTokenno();
                }
                if(hours==19 && minutes>30) {
                    Toast.makeText(context, "Appointment Unavailable", Toast.LENGTH_SHORT).show();
                }
                else {
                    builder.setTitle("Book Appointment")
                            .setMessage("Your token no is " + token + " and time slot is " + timeslot + ". Do you want to confirm the appointment?")
                            .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int which) {
                                    Toast.makeText(context, "Appointment Confirmed", Toast.LENGTH_SHORT).show();
                                    appointment6.setClickable(false);
                                    appointment6.setAlpha(.5f);
                                }
                            })
                            .setNegativeButton(android.R.string.no, new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int which) {
                                    token--;
                                    hours = hours1 - hours2;
                                    minutes = minutes1;
                                    if (minutes > 59) {
                                        minutes = minutes - 60;
                                        hours = hours + 1;
                                        if (hours > 23) {
                                            hours = hours - 24;
                                        }
                                    } else {

                                        if (hours > 23) {
                                            hours = hours - 24;
                                        }
                                    }
                                    timeslot = String.valueOf(hours) + ":" + String.valueOf(minutes);
                                }
                            })
                            .show();
                }
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.options_menu, menu);
        //getMenuInflater().inflate(R.menu.popup_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == R.id.menu_signout) {
            Toast.makeText(this, "Sign Out", Toast.LENGTH_SHORT).show();
            firebaseAuth.getInstance().signOut();
            startActivity(new Intent(Dashboard.this, LoginActivity.class));
        }

        if (item.getItemId() == R.id.menu_locate) {
            startActivity(new Intent(Dashboard.this, MapActivity.class));
        }

        if (item.getItemId() == R.id.menu_about) {
            startActivity(new Intent(Dashboard.this, AboutUsActivity.class));
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    public void showPopUp(){
        View menuItemView = null;
        if(buttonClicked == 0) {
            menuItemView = findViewById(R.id.btn_reports);
        }
        else if(buttonClicked == 1) {
            menuItemView = findViewById(R.id.btn_reports2);
        }
        else if(buttonClicked == 2) {
            menuItemView = findViewById(R.id.btn_reports3);
        }
        else if(buttonClicked == 3) {
            menuItemView = findViewById(R.id.btn_reports4);
        }
        else if(buttonClicked == 4) {
            menuItemView = findViewById(R.id.btn_reports5);
        }
        else{
            menuItemView = findViewById(R.id.btn_reports6);
        }
        popup = new PopupMenu(getApplicationContext(), menuItemView);
        MenuInflater inflate = popup.getMenuInflater();
        inflate.inflate(R.menu.popup_menu, popup.getMenu());
        popup.show();

    }

    @Override
    public boolean onMenuItemClick(MenuItem item) {
        return false;
    }
       /*@Override
    public boolean onMenuItemClick(MenuItem item) {
        int id = item.getItemId();
        switch (id){
            case R.id.menu_uploadReport:
                Toast.makeText(getApplicationContext(), "Upload Report", Toast.LENGTH_SHORT).show();
                return  true;
        }
        *//*Intent i = new Intent(getApplicationContext(), Upload.class);
                i.putExtra("child_document_id",_idFamilyMember[0]);
                i.putExtra("doc_id",docID);
                startActivity(i);*//*
        return false;
    }*/
}

