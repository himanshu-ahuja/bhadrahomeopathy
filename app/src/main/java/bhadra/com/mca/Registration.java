package bhadra.com.mca;

import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.util.Patterns;
import android.view.Gravity;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RelativeLayout.LayoutParams;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.firestore.FirebaseFirestore;

public class Registration extends AppCompatActivity {

    //action bar
    ActionBar actionbar;
    TextView textTitle;
    android.app.ActionBar.LayoutParams layoutparams;
    ProgressDialog dialog;
    private EditText textFamilyHeadNameSignUp;
    private EditText textEmailSignUp;
    private EditText textAddress1SignUp;
    private EditText textAddress2SignUp;
    private EditText textZipCodeSignUp;
    private EditText textMobileNumberSignUp;
    private RadioGroup radioGroupGenderSignUp;
    private RadioButton radioButtonMaleSignUp;
    private RadioButton radioButtonFemaleSignUp;
    private CheckBox checkBoxDisclaimerSignUp;
    private CheckBox checkBoxTermsSignUp;
    private Button buttonCancel;
    private Button buttonRegister;
    private boolean validate = true;
    private String gender = "";
    private FirebaseAuth firebaseAuth;
    private FirebaseFirestore db;

    private void ActionBarTitleGravity() {
        actionbar = getSupportActionBar();
        textTitle = new TextView(getApplicationContext());
        layoutparams = new android.app.ActionBar.LayoutParams(LayoutParams.MATCH_PARENT, ActionBar.LayoutParams.WRAP_CONTENT);
        textTitle.setLayoutParams(layoutparams);
        textTitle.setText(R.string.sign_up);
        textTitle.setTextColor(Color.WHITE);
        textTitle.setGravity(Gravity.CENTER_HORIZONTAL);
        textTitle.setTextSize(17);
        actionbar.setDisplayOptions(ActionBar.DISPLAY_SHOW_CUSTOM);
        actionbar.setCustomView(textTitle);
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registration);

        ActionBarTitleGravity();

        textFamilyHeadNameSignUp = findViewById(R.id.editTextFamilyHeadNameSignUp);
        textEmailSignUp = findViewById(R.id.editTextEmailSignUp);
        textAddress1SignUp = findViewById(R.id.editTextAddress1SignUp);
        textAddress2SignUp = findViewById(R.id.editTextAddress2SignUp);
        textZipCodeSignUp = findViewById(R.id.editTextZipCodeSignUp);
        textMobileNumberSignUp = findViewById(R.id.editTextMobileNumberSignUp);
        radioGroupGenderSignUp = findViewById(R.id.RadioGroupGenderSignUp);
        radioButtonMaleSignUp = findViewById(R.id.radioButtonSignUpMale);
        radioButtonFemaleSignUp = findViewById(R.id.radioButtonSignUpFemale);
        checkBoxDisclaimerSignUp = findViewById(R.id.checkBoxDisclaimerSignUp);
        checkBoxTermsSignUp = findViewById(R.id.checkBoxTermsSignUp);
        buttonRegister = findViewById(R.id.buttonSubmitSignUp);
        buttonCancel = findViewById(R.id.buttonCancelSignUp);

        firebaseAuth = FirebaseAuth.getInstance();
        db = FirebaseFirestore.getInstance();

        buttonRegister.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                final String userEmail = textEmailSignUp.getText().toString().trim();
                if (!TextUtils.isEmpty(userEmail)) {
                    validate = android.util.Patterns.EMAIL_ADDRESS.matcher(userEmail).matches();
                    if (validate == false) {
                        Toast.makeText(getApplicationContext(), "Please enter valid email", Toast.LENGTH_SHORT).show();
                        return;
                    }
                }
                //user details
                final String familyHeadName = textFamilyHeadNameSignUp.getText().toString().trim();

                if (radioButtonMaleSignUp.isChecked()) {
                    gender = "Male";
                }
                if (radioButtonFemaleSignUp.isChecked()) {
                    gender = "Female";
                }
                final String address1 = textAddress1SignUp.getText().toString().trim();
                final String address2 = textAddress2SignUp.getText().toString().trim();
                final String zipCode = textZipCodeSignUp.getText().toString().trim();
                final String mobileNumber = textMobileNumberSignUp.getText().toString().trim();

                if (!TextUtils.isEmpty(mobileNumber)) {
                    validate = Patterns.PHONE.matcher(mobileNumber).matches();
                    if (validate == false) {
                        Toast.makeText(getApplicationContext(), "Please enter valid mobile number", Toast.LENGTH_SHORT).show();
                        return;
                    }
                }
                validate = validateInput(userEmail, familyHeadName, gender, address1, address2, zipCode, mobileNumber);

                if (validate == false) {
                    return;
                }
                if (validate == true) {
                    checkUserExists(mobileNumber, familyHeadName, gender, userEmail, address1, address2, zipCode, false);
                }
            }
        });

        buttonCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(Registration.this, LoginActivity.class));
                finish();
            }
        });
    }

    public void checkUserExists(final String mobileNumber, final String familyHeadName, final String gender, final String userEmail, final String address1, final String address2, final String zipCode, final boolean whichActivity) {
        dialog = ProgressDialog.show(this, "Registration", "Creating User", true);

        DatabaseReference databaseReference = FirebaseDatabase.getInstance().getReference();
        databaseReference.child("users")
                .orderByChild("mobileNumber")
                .equalTo(mobileNumber)
                .addValueEventListener(new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {
                        if (dataSnapshot.exists()) {
                            textMobileNumberSignUp.setError("Account with this phone number already exists!");
                            if(dialog!= null && dialog.isShowing())
                                dialog.dismiss();
                            return;
                        } else {
                            //Toast.makeText(getApplicationContext(), "Creating new user", Toast.LENGTH_SHORT).show();
                            if(dialog!= null && dialog.isShowing())
                                dialog.dismiss();
                            Intent intent = new Intent();
                            intent.setClass(Registration.this, OTPAuth.class);
                            intent.putExtra("mobileNumber", mobileNumber);
                            intent.putExtra("headName", familyHeadName);
                            intent.putExtra("gender", gender);
                            intent.putExtra("email", userEmail);
                            intent.putExtra("address1", address1);
                            intent.putExtra("address2", address2);
                            intent.putExtra("zipCode", zipCode);
                            intent.putExtra("fromLoginActivity", whichActivity);
                            startActivity(intent);
                        }
                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {
                        if(dialog!= null && dialog.isShowing())
                            dialog.dismiss();
                        Toast.makeText(getApplicationContext(), "Firebase error occurred", Toast.LENGTH_SHORT).show();
                        return;
                    }
                });
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    public boolean validateInput(String email, String head_name, String gender, String address1, String address2, String zipCode, String mobileNumber) {

        if (TextUtils.isEmpty(head_name)) {
            textFamilyHeadNameSignUp.setError("Family Head Name is required");
            return false;
        }

        if (TextUtils.isEmpty(gender)) {
            Toast.makeText(this, "Please select your gender", Toast.LENGTH_SHORT).show();
            return false;
        }
        if (TextUtils.isEmpty(email)) {
            textEmailSignUp.setError("Email is required");
            return false;
        }
        if (TextUtils.isEmpty(address1)) {
            textAddress1SignUp.setError("Address is required");
            return false;
        }
        if (TextUtils.isEmpty(zipCode)) {
            textZipCodeSignUp.setError("Zip Code is required");
            return false;
        }
        if (TextUtils.isEmpty(mobileNumber)) {
            textMobileNumberSignUp.setError("Mobile Number is required");
            return false;
        }
        if (mobileNumber.length() < 10) {
            textMobileNumberSignUp.setError("Mobile Number is not valid");
            return false;
        }
        if(!checkBoxDisclaimerSignUp.isChecked()){
            checkBoxDisclaimerSignUp.setError("You must agree to the Disclaimer");
            return false;
        }
        if(!checkBoxTermsSignUp.isChecked()){
            checkBoxTermsSignUp.setError("You must agree to the Terms & Condition");
            return false;
        }


        return true;
    }

    @Override
    public void onBackPressed()
    {

        Intent mainIntent = new Intent();
        mainIntent.setClass(Registration.this, LoginActivity.class);
        startActivity(mainIntent);
        finish();
    }
}