package bhadra.com.mca;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.ContentResolver;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.os.ParcelFileDescriptor;
import android.provider.OpenableColumns;
import android.support.annotation.NonNull;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.webkit.MimeTypeMap;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;

import java.io.FileDescriptor;
import java.io.IOException;

public class Upload extends AppCompatActivity {


    // Root Database Name for Firebase Database.
    public static final String Database_Path = "gs://bhadra-homeopathy.appspot.com";
    private static final int READ_REQUEST_CODE = 42;
    TextView textView1, textView2, textView3, textView4, textView5, textView6, textView7, textView8, textView9, textView10, type1, type2, type3, type4, type5, type6, type7, type8, type9, type10;
    ImageView imgview1, imgview2, imgview3, imgview4, imgview5, imgview6, imgview7, imgview8, imgview9, imgview10;
    Button buttonUploadImage;
    Bitmap img, pdf;
    int clicks = 0;
    String typeFile;
    String uriString;
    String[] mimeTypes = {"application/pdf", "application/zip", "image/*"};
    // Folder path for Firebase Storage.
    String Storage_Path = "Reports_Upload/";
    // Creating URI.
    Uri FilePathUri;
    Uri uri;

    // Creating StorageReference and DatabaseReference object.
    StorageReference storageReference;
    DatabaseReference databaseReference;

    // Image request code for onActivityResult() .
    int Image_Request_Code = 7;

    ProgressDialog progressDialog ;

    private String child_document_id;
    private String doc_id;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_upload);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);


        textView1 = (TextView) findViewById(R.id.filename1);
        textView2 = (TextView) findViewById(R.id.filename2);
        textView3 = (TextView) findViewById(R.id.filename3);
        textView4 = (TextView) findViewById(R.id.filename4);
        textView5 = (TextView) findViewById(R.id.filename5);
        textView6 = (TextView) findViewById(R.id.filename6);
        textView7 = (TextView) findViewById(R.id.filename7);
        textView8 = (TextView) findViewById(R.id.filename8);
        textView9 = (TextView) findViewById(R.id.filename9);
        textView10 = (TextView) findViewById(R.id.filename10);
        buttonUploadImage = findViewById(R.id.buttonUploadImage);
        type1 = (TextView) findViewById(R.id.filetype1);
        type2 = (TextView) findViewById(R.id.filetype2);
        type3 = (TextView) findViewById(R.id.filetype3);
        type4 = (TextView) findViewById(R.id.filetype4);
        type5 = (TextView) findViewById(R.id.filetype5);
        type6 = (TextView) findViewById(R.id.filetype6);
        type7 = (TextView) findViewById(R.id.filetype7);
        type8 = (TextView) findViewById(R.id.filetype8);
        type9 = (TextView) findViewById(R.id.filetype9);
        type10 = (TextView) findViewById(R.id.filetype10);
        imgview1 = (ImageView) findViewById(R.id.img1);
        imgview2 = (ImageView) findViewById(R.id.img2);
        imgview3 = (ImageView) findViewById(R.id.img3);
        imgview4 = (ImageView) findViewById(R.id.img4);
        imgview5 = (ImageView) findViewById(R.id.img5);
        imgview6 = (ImageView) findViewById(R.id.img6);
        imgview7 = (ImageView) findViewById(R.id.img7);
        imgview8 = (ImageView) findViewById(R.id.img8);
        imgview9 = (ImageView) findViewById(R.id.img9);
        imgview10 = (ImageView) findViewById(R.id.img10);

        pdf = BitmapFactory.decodeResource(getResources(), R.drawable.pdf);

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                clicks++;
                performFileSearch();
            }
        });

        // Assign FirebaseStorage instance to storageReference.
        storageReference = FirebaseStorage.getInstance().getReference();

        // Assign FirebaseDatabase instance with root database name.
        databaseReference = FirebaseDatabase.getInstance().getReference();
        // Assigning Id to ProgressDialog.
        progressDialog = new ProgressDialog(Upload.this);

        buttonUploadImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                    if(textView1.getText().toString() == ""){
                        Toast.makeText(getApplicationContext(),"Please select an image to upload",Toast.LENGTH_SHORT).show();
                        return;
                    }
                FilePathUri = uri;
                UploadImageFileToFirebaseStorage();
            }
        });
    }

    // Creating Method to get the selected image file Extension from File Path URI.
    public String GetFileExtension(Uri uri) {

        ContentResolver contentResolver = getContentResolver();
        MimeTypeMap mimeTypeMap = MimeTypeMap.getSingleton();
        // Returning the file Extension.
        return mimeTypeMap.getExtensionFromMimeType(contentResolver.getType(uri)) ;

    }
    // Creating UploadImageFileToFirebaseStorage method to upload image on storage.
    public void UploadImageFileToFirebaseStorage() {
        if (FilePathUri != null) {
            // Setting progressDialog Title.
            progressDialog.setTitle("Uploading report..");
            // Showing progressDialog.
            progressDialog.show();

            child_document_id = getIntent().getExtras().getString("child_document_id");
            doc_id = getIntent().getExtras().getString("doc_id");
            Log.d("child:",child_document_id);
            Log.d("doc_id:",doc_id);

            // Creating second StorageReference.
            //StorageReference storageReference2nd = storageReference.child(Storage_Path + System.currentTimeMillis() + "." + GetFileExtension(FilePathUri));
            StorageReference storageReference1 = storageReference.child(Storage_Path + child_document_id + "." + GetFileExtension(FilePathUri));

            // Adding addOnSuccessListener to second StorageReference.
            storageReference1.putFile(FilePathUri)
                    .addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
                        @Override
                        public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                            // Getting image name from EditText and store into string variable.
                            String TempImageName = textView1.getText().toString().trim();
                            // Hiding the progressDialog after done uploading.
                            progressDialog.dismiss();
                            // Showing toast message after done uploading.
                            Toast.makeText(getApplicationContext(), "Image Uploaded Successfully ", Toast.LENGTH_LONG).show();

                            // Adding image upload id s child element into databaseReference.
                            if(doc_id.equals(child_document_id)){
                                @SuppressWarnings("VisibleForTests")
                                AddUser imageUploadInfo = new AddUser(TempImageName, taskSnapshot.getDownloadUrl().toString());
                                // Getting image upload ID.
                                DatabaseReference imageRef = FirebaseDatabase.getInstance().getReference();
                                String ImageUploadId = databaseReference.push().getKey();
                                Log.d("IMAGEID:",ImageUploadId);
                                databaseReference.child("users").child(doc_id).child("reports").push().setValue(imageUploadInfo);
                                startActivity(new Intent(Upload.this, Dashboard.class));
                            }
                            else {
                                @SuppressWarnings("VisibleForTests")
                                AddPatient imageUploadInfo = new AddPatient(TempImageName, taskSnapshot.getDownloadUrl().toString());
                                // Getting image upload ID.
                                DatabaseReference imageRef = FirebaseDatabase.getInstance().getReference();
                                String ImageUploadId = databaseReference.push().getKey();
                                Log.d("IMAGEID:",ImageUploadId);
                                databaseReference.child("users").child(doc_id).child("familyMembers").child(child_document_id).child("reports").push().setValue(imageUploadInfo);
                                startActivity(new Intent(Upload.this, Dashboard.class));
                            }//databaseReference.child("users").child(FirebaseAuth.getInstance().getCurrentUser().getUid()).setValue(imageUploadInfo);
                        }
                    })
                    .addOnFailureListener(new OnFailureListener() {
                        @Override
                        public void onFailure(@NonNull Exception e) {

                        }
                    });
        }
    }


    public void performFileSearch() {

        Intent intent = new Intent(Intent.ACTION_OPEN_DOCUMENT);
        intent.addCategory(Intent.CATEGORY_OPENABLE);
        intent.setType("image/*");
        intent.putExtra(Intent.EXTRA_MIME_TYPES, mimeTypes);
        startActivityForResult(intent, READ_REQUEST_CODE);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent resultData) {


        if (requestCode == READ_REQUEST_CODE && resultCode == Activity.RESULT_OK) {
           uri = null;
            if (resultData != null) {
                uri = resultData.getData();
            }
            if (clicks <= 10) {
                try {
                    img = getBitmapFromUri(uri);
                } catch (IOException e) {
                }
            }
            dumpImageMetaData(uri, clicks, img);
        }
    }

    private Bitmap getBitmapFromUri(Uri uri) throws IOException {
        ParcelFileDescriptor parcelFileDescriptor =
                getContentResolver().openFileDescriptor(uri, "r");
        FileDescriptor fileDescriptor = parcelFileDescriptor.getFileDescriptor();
        Bitmap image = BitmapFactory.decodeFileDescriptor(fileDescriptor);
        parcelFileDescriptor.close();
        return image;
    }

    public void dumpImageMetaData(Uri uri, int count, Bitmap image) {

        Cursor cursor = getApplicationContext().getContentResolver()
                .query(uri, null, null, null, null, null);

        try {
            if (cursor != null && cursor.moveToFirst()) {

                String displayName = cursor.getString(
                        cursor.getColumnIndex(OpenableColumns.DISPLAY_NAME));
                Log.i("info", "Display Name: " + displayName);
                String[] splits = displayName.split("\\.");
                int len = splits.length;
                String type = splits[len - 1];
                switch (count) {
                    case 1:
                        textView1.setText(displayName);
                        type1.setText(type);
                        if (type.equals("pdf")) {
                            imgview1.setImageBitmap(pdf);
                        } else {
                            imgview1.setImageBitmap(image);
                        }

                        break;

                    case 2:
                        textView2.setText(displayName);
                        type2.setText(type);
                        if (type.equals("pdf")) {
                            imgview2.setImageBitmap(pdf);
                        } else {
                            imgview2.setImageBitmap(image);
                        }

                        break;

                    case 3:
                        textView3.setText(displayName);
                        type3.setText(type);
                        if (type.equals("pdf")) {
                            imgview3.setImageBitmap(pdf);
                        } else {
                            imgview3.setImageBitmap(image);
                        }
                        break;

                    case 4:
                        textView4.setText(displayName);
                        type4.setText(type);
                        if (type.equals("pdf")) {
                            imgview4.setImageBitmap(pdf);
                        } else {
                            imgview4.setImageBitmap(image);
                        }
                        break;

                    case 5:
                        textView5.setText(displayName);
                        type5.setText(type);
                        if (type.equals("pdf")) {
                            imgview5.setImageBitmap(pdf);
                        } else {
                            imgview5.setImageBitmap(image);
                        }
                        break;

                    case 6:
                        textView6.setText(displayName);
                        type6.setText(type);
                        if (type.equals("pdf")) {
                            imgview6.setImageBitmap(pdf);
                        } else {
                            imgview6.setImageBitmap(image);
                        }
                        break;

                    case 7:
                        textView7.setText(displayName);
                        type7.setText(type);
                        if (type.equals("pdf")) {
                            imgview7.setImageBitmap(pdf);
                        } else {
                            imgview7.setImageBitmap(image);
                        }
                        break;

                    case 8:
                        textView8.setText(displayName);
                        type8.setText(type);
                        if (type.equals("pdf")) {
                            imgview8.setImageBitmap(pdf);
                        } else {
                            imgview8.setImageBitmap(image);
                        }
                        break;

                    case 9:
                        textView9.setText(displayName);
                        type9.setText(type);
                        if (type.equals("pdf")) {
                            imgview9.setImageBitmap(pdf);
                        } else {
                            imgview9.setImageBitmap(image);
                        }
                        break;

                    case 10:
                        textView10.setText(displayName);
                        type10.setText(type);
                        if (type.equals("pdf")) {
                            imgview10.setImageBitmap(pdf);
                        } else {
                            imgview10.setImageBitmap(image);
                        }
                        break;

                    default:
                        Toast.makeText(getApplicationContext(), "File Limit Exceeded", Toast.LENGTH_SHORT).show();
                        break;
                }
            }

        } finally {
            cursor.close();
        }
    }
}
