package bhadra.com.mca;

import android.content.Intent;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.TextPaint;
import android.text.method.LinkMovementMethod;
import android.text.style.ClickableSpan;
import android.view.Gravity;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.TextView;



public class AboutUsActivity extends AppCompatActivity {
    //action bar
    ActionBar actionbar;
    TextView textTitle;
    android.app.ActionBar.LayoutParams layoutparams;


    private void ActionBarTitleGravity() {
        actionbar = getSupportActionBar();
        textTitle = new TextView(getApplicationContext());
        layoutparams = new android.app.ActionBar.LayoutParams(RelativeLayout.LayoutParams.MATCH_PARENT, ActionBar.LayoutParams.WRAP_CONTENT);
        textTitle.setLayoutParams(layoutparams);
        textTitle.setText(R.string.about);
        textTitle.setTextColor(Color.WHITE);
        textTitle.setGravity(Gravity.CENTER_HORIZONTAL);
        textTitle.setTextSize(17);
        actionbar.setDisplayOptions(ActionBar.DISPLAY_SHOW_CUSTOM);
        actionbar.setCustomView(textTitle);
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_about_us);

        ActionBarTitleGravity();

        SpannableString ss = new SpannableString("You may contact us at our email address:doctor@bhadrahomeo.com");
        ClickableSpan clickableSpan = new ClickableSpan() {
            @Override
            public void onClick(View textView) {
                Intent i = new Intent(Intent.ACTION_SEND);
                i.setData(Uri.parse("email"));
                String[] s = {"doctor@bhadrahomeo.com"};
                i.putExtra(Intent.EXTRA_EMAIL, s);
                i.putExtra(Intent.EXTRA_SUBJECT, "");
                i.putExtra(Intent.EXTRA_TEXT, "");
                i.setType("message/rfc822");
                Intent chooser = Intent.createChooser(i, "Launch Email");
                startActivity(chooser);
            }

            @Override
            public void updateDrawState(TextPaint ds) {
                super.updateDrawState(ds);
                ds.setUnderlineText(true);
            }
        };
        ss.setSpan(clickableSpan, 40, 62, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
        TextView textView = (TextView) findViewById(R.id.textlink);
        textView.setText(ss);
        textView.setMovementMethod(LinkMovementMethod.getInstance());
        textView.setHighlightColor(Color.TRANSPARENT);

        SpannableString ss2 = new SpannableString("You may call us at our phone number:+918025220133");
        ClickableSpan clickableSpan2 = new ClickableSpan() {
            @Override
            public void onClick(View textView2) {
                Intent i = new Intent(Intent.ACTION_DIAL);
                {
                    i.setData(Uri.parse("tel:+918025220133"));
                }
                startActivity(i);
            }

            @Override
            public void updateDrawState(TextPaint ds2) {
                super.updateDrawState(ds2);
                ds2.setUnderlineText(true);
            }
        };
        ss2.setSpan(clickableSpan2, 36, 49, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);

        TextView textView2 = (TextView) findViewById(R.id.textlink1);
        textView2.setText(ss2);
        textView2.setMovementMethod(LinkMovementMethod.getInstance());
        textView2.setHighlightColor(Color.TRANSPARENT);

    }
    }
