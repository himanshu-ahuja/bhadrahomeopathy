package bhadra.com.mca;
import java.nio.ByteBuffer;
import java.util.UUID;

/**
 * Created by himanshu on 4/3/18.
 */

public class GenerateUUID {

    public static String GenerateNumber(){
        UUID uuid = UUID.randomUUID();
        long l = ByteBuffer.wrap(uuid.toString().getBytes()).getLong();
        String id =  Long.toString(l, Character.MAX_RADIX);
        return id;
    }
}
