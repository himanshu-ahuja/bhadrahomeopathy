package bhadra.com.mca;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

public class DisplayReport extends AppCompatActivity {
    StorageReference storageReference;
    DatabaseReference databaseReference;
    DatabaseReference imageRef;
    // Image request code for onActivityResult() .
    int Image_Request_Code = 7;
    ProgressDialog progressDialog ;
    // Creating RecyclerView.
    RecyclerView recyclerView;
    ImageView imageView;
    TextView textView;
    // Creating RecyclerView.Adapter.
    RecyclerView.Adapter adapter ;
    // Creating List of ImageUploadInfo class.
    List<AddUser> list = new ArrayList<>();
    private String child_document_id;
    private String doc_id;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_display_report);

        imageView = findViewById(R.id.imageView);
        textView = findViewById(R.id.ImageNameTextView);
        textView.setVisibility(View.GONE);
        child_document_id = getIntent().getExtras().getString("child_document_id");
        doc_id = getIntent().getExtras().getString("doc_id");

        storageReference = FirebaseStorage.getInstance().getReference();
        // Assign FirebaseDatabase instance with root database name.
        databaseReference = FirebaseDatabase.getInstance().getReference();
        imageRef = databaseReference.child("users").child(doc_id).child("reports");

        // Assigning Id to ProgressDialog.
        progressDialog = new ProgressDialog(DisplayReport.this);

        Log.d("child:",child_document_id);
        Log.d("doc_id:",doc_id);

        imageRef.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                Log.d("REPORT_CHILD:", "" + String.valueOf(dataSnapshot.getChildrenCount()) + "");
                for(DataSnapshot imageSnapshot : dataSnapshot.getChildren()){
                    AddUser userReport = imageSnapshot.getValue(AddUser.class);
                    Log.d("IMAGE_NAME:", "" + userReport.getImageName() + "");
                    String image_name = userReport.getImageName();
                    Log.d("IMAGE_URL:", "" + userReport.getImageURL() + "");
                    String uri = userReport.getImageURL();
                    Log.d("URI:", "" + uri + "");
                    Picasso.get().load(uri).into(imageView);
                    textView.setVisibility(View.VISIBLE);
                    textView.setText(""+image_name);
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }
}
