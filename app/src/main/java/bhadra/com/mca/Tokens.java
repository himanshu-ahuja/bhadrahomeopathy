package bhadra.com.mca;

/**
 * Created by himanshu on 10/3/18.
 */

public class Tokens {
    public int tokenno;
    public String time;


    public Tokens() {

    }

    public int getTokenno() {
        return tokenno;
    }

    public void setTokenno(int tokenno) {
        this.tokenno = tokenno;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }
}
