package bhadra.com.mca;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.FirebaseException;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseAuthInvalidCredentialsException;
import com.google.firebase.auth.PhoneAuthCredential;
import com.google.firebase.auth.PhoneAuthProvider;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.util.concurrent.TimeUnit;

public class OTPAuth extends AppCompatActivity {
    private String mobileNumber = "";
    private String SF_NAME = "docID";
    private EditText editTextMobile;
    private EditText editTextOTP;
    private TextView textViewError;
    private Button buttonSendOTP;
    private Button buttonResendOTP;
    private Button buttonVerifyOTP;

    private FirebaseAuth firebaseAuth;
    private DatabaseReference databaseReference;

    private PhoneAuthProvider.OnVerificationStateChangedCallbacks mCallbacks;
    private String mVerificationId;
    private PhoneAuthProvider.ForceResendingToken mResendToken;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_otpauth);

        editTextMobile = findViewById(R.id.editTextMobileNumber);
        editTextOTP = findViewById(R.id.editTextOTP);
        textViewError = findViewById(R.id.textViewError);
        buttonSendOTP = findViewById(R.id.buttonSendOTP);
        buttonResendOTP = findViewById(R.id.buttonResendOTP);
        buttonVerifyOTP = findViewById(R.id.buttonVerifyOTP);

        firebaseAuth = FirebaseAuth.getInstance();
        databaseReference = FirebaseDatabase.getInstance().getReference();

        Intent OTPIntent = this.getIntent();
        if (OTPIntent != null) {
            mobileNumber = OTPIntent.getExtras().getString("mobileNumber");
            editTextMobile.setText(mobileNumber);
        }

        buttonSendOTP.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                buttonSendOTP.setEnabled(false);
                buttonSendOTP.setVisibility(View.GONE);
                buttonVerifyOTP.setEnabled(true);
                buttonVerifyOTP.setVisibility(View.VISIBLE);
                buttonResendOTP.setEnabled(true);
                buttonResendOTP.setVisibility(View.VISIBLE);
                PhoneAuthProvider.getInstance().verifyPhoneNumber(
                        mobileNumber,
                        60,
                        TimeUnit.SECONDS,
                        OTPAuth.this,
                        mCallbacks
                );


            }
        });

        buttonResendOTP.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                buttonResendOTP.setEnabled(false);
                textViewError.setVisibility(View.GONE);
                resendVerificationCode(mobileNumber, mResendToken);
            }
        });

        mCallbacks = new PhoneAuthProvider.OnVerificationStateChangedCallbacks() {
            @Override
            public void onVerificationCompleted(PhoneAuthCredential phoneAuthCredential) {
                signInWithPhoneAuthCredential(phoneAuthCredential);
            }

            @Override
            public void onVerificationFailed(FirebaseException e) {
                Log.d("Auth", "Message: " + e.getStackTrace() + "");
                textViewError.setText("Invalid Number Entered");
                textViewError.setVisibility(View.VISIBLE);
            }

            @Override
            public void onCodeSent(String verificationId,
                                   PhoneAuthProvider.ForceResendingToken token) {
                mVerificationId = verificationId;
                mResendToken = token;
                buttonVerifyOTP.setVisibility(View.VISIBLE);
                buttonSendOTP.setVisibility(View.GONE);
            }

        };


        buttonVerifyOTP.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String verificationCode = editTextOTP.getText().toString();
                if (verificationCode.equals("")) {
                    editTextOTP.setError("Enter OTP");
                    return;
                } else {
                    PhoneAuthCredential phoneAuthCredential = PhoneAuthProvider.getCredential(mVerificationId, verificationCode);
                    signInWithPhoneAuthCredential(phoneAuthCredential);
                    buttonVerifyOTP.setEnabled(false);
                    buttonResendOTP.setEnabled(false);
                }
            }
        });

    }


    @Override
    public boolean onSupportNavigateUp() {
        finish();
        return true;
    }

    private void signInWithPhoneAuthCredential(PhoneAuthCredential credential) {
        firebaseAuth.signInWithCredential(credential)
                .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {

                        if (task.isSuccessful()) {
                            // Sign in success, update UI with the signed-in user's information

                            boolean fromLogin = getIntent().getExtras().getBoolean("fromLoginActivity");
                            if (fromLogin == false) {

                                writeNewUser(
                                        getIntent().getExtras().getString("headName"),
                                        getIntent().getExtras().getString("gender"),
                                        getIntent().getExtras().getString("email"),
                                        getIntent().getExtras().getString("address1"),
                                        getIntent().getExtras().getString("address2"),
                                        getIntent().getExtras().getString("zipCode"),
                                        mobileNumber);
                            } else {
                                Log.d("NUMBER:", mobileNumber);
                                Intent loginIntent = new Intent(OTPAuth.this, Dashboard.class);
                                startActivity(loginIntent);
                                finish();
                            }


                            // ...
                        } else {
                            // Sign in failed, display a message and update the UI
                            textViewError.setText("Invalid OTP Entered");
                            textViewError.setVisibility(View.VISIBLE);

                            if (task.getException() instanceof FirebaseAuthInvalidCredentialsException) {
                                // The verification code entered was invalid
                            }
                        }
                    }
                });
    }

    //resend otp
    private void resendVerificationCode(String phoneNumber,
                                        PhoneAuthProvider.ForceResendingToken token) {
        PhoneAuthProvider.getInstance().verifyPhoneNumber(
                mobileNumber,        // Phone number to verify
                60,                 // Timeout duration
                TimeUnit.SECONDS,   // Unit of timeout
                this,               // Activity (for callback binding)
                mCallbacks,         // OnVerificationStateChangedCallbacks
                token);             // ForceResendingToken from callbacks
    }


    private void writeNewUser(String head_name, String gender, String email, String address1, String address2, String zipCode, String mobileNumber) {
        AddUser user = new AddUser(head_name, gender, email, address1, address2, zipCode, mobileNumber);
        final FamilyPatientCount familyPatientCount = new FamilyPatientCount();
        DatabaseReference rootRef = databaseReference.child("users");
        final DatabaseReference userRef = rootRef.push();
        userRef.setValue(user)
                .addOnSuccessListener(new OnSuccessListener<Void>() {
                    @Override
                    public void onSuccess(Void aVoid) {

                        SharedPreferences pref = getSharedPreferences(SF_NAME, MODE_PRIVATE);
                        SharedPreferences.Editor editor = pref.edit();
                        editor.putString("documentID", userRef.getKey());
                        editor.apply();
                        //familyPatientCount.setUserKey(userRef.getKey());
                        Toast.makeText(getApplicationContext(), "Registration Success!", Toast.LENGTH_SHORT).show();
                        Intent loginIntent = new Intent(OTPAuth.this, Dashboard.class);
                        startActivity(loginIntent);
                        finish();
                    }
                })
                .addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        Log.d("Registration: ", "Error was: " + e.getMessage() + "");
                        Toast.makeText(getApplicationContext(), "Could not complete registraton. Please try again later", Toast.LENGTH_SHORT).show();
                    }
                });

    }
}



