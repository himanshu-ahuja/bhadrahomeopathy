package bhadra.com.mca;

/**
 * Created by himanshu on 28/2/18.
 */
public class AddUser {



    public String imageName;
    public String imageURL;
    private String familyHeadName;
    private String gender;
    private String address1;
    private String address2;
    private String zipcode;
    private String mobileNumber;
    private String email;

    public AddUser(){
        // Default constructor required for calls to DataSnapshot.getValue(User.class)
    }

    public AddUser(String familyHeadName, String gender, String email, String address1, String address2, String zipcode, String mobileNumber){
        this.familyHeadName = familyHeadName;
        this.gender = gender;
        this.email = email;
        this.address1 = address1;
        this.address2 = address2;
        this.zipcode = zipcode;
        this.mobileNumber = mobileNumber;
    }

    public AddUser(String name, String url) {

        this.imageName = name;
        this.imageURL= url;
    }

    public String getImageName() {
        return imageName;
    }

    public String getImageURL() {
        return imageURL;
    }

    public String getFamilyHeadName() {
        return familyHeadName;
    }

    public void setFamilyHeadName(String familyHeadName) {
        this.familyHeadName = familyHeadName;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getAddress1() {
        return address1;
    }

    public void setAddress1(String address1) {
        this.address1 = address1;
    }

    public String getAddress2() {
        return address2;
    }

    public void setAddress2(String address2) {
        this.address2 = address2;
    }

    public String getZipcode() {
        return zipcode;
    }

    public void setZipcode(String zipcode) {
        this.zipcode = zipcode;
    }

    public String getMobileNumber() {
        return mobileNumber;
    }

    public void setMobileNumber(String mobileNumber) {
        this.mobileNumber = mobileNumber;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }
}
