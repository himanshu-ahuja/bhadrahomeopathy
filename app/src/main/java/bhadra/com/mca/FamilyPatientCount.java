package bhadra.com.mca;

/**
 * Created by himanshu on 8/3/18.
 */

public class FamilyPatientCount {

    private int familyPatientCount = 3;
    private String userKey = "";

    public  FamilyPatientCount(){

    }

    public int getFamilyPatientCount() {
        return familyPatientCount;
    }

    public void setFamilyPatientCount(int familyPatientCount) {
        this.familyPatientCount = familyPatientCount;
    }

    public String getUserKey() {
        return userKey;
    }

    public void setUserKey(String userKey) {
        this.userKey = userKey;
    }
}
