package bhadra.com.mca;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

import static bhadra.com.mca.R.id.map;

public class MapActivity extends AppCompatActivity implements  OnMapReadyCallback {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_map);
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(map);
        mapFragment.getMapAsync( this);
    }

    public void onMapReady(GoogleMap googleMap) {
        LatLng bangalore = new LatLng(12.956237, 77.654876);
        googleMap.addMarker(new MarkerOptions().position(bangalore)
                .title("Marker in Bhadra HomoeoPathic"));
        googleMap.moveCamera(CameraUpdateFactory.newLatLngZoom(bangalore,18));
    }
    }

